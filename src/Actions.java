public interface Actions {
    void fillCell(int movesCounter, Field field, int verticalCoord, int horizontalCoord);
}
