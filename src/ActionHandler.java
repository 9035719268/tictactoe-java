import java.util.Scanner;

public class ActionHandler implements Actions {

    @Override
    public void fillCell(int movesCounter, Field field, int verticalIndex, int horizontalIndex) {
        char fillerSymbol = (movesCounter % 2 == 0) ? 'O' : 'X';
        try {
            field.fieldArea[verticalIndex][horizontalIndex] = field.fieldArea[verticalIndex][horizontalIndex];
            if (field.isCellNotEmpty(verticalIndex, horizontalIndex)) {
                System.out.println("Error. Cell is not empty. Try again.");
                Scanner in = new Scanner(System.in);
                System.out.print(fillerSymbol + ", enter vertical index: ");
                int verticalIndex_new = in.nextInt();
                System.out.print(fillerSymbol + ", enter horizontal index: ");
                int horizontalIndex_new = in.nextInt();
                fillCell(movesCounter, field, verticalIndex_new, horizontalIndex_new);
            }
            field.fieldArea[verticalIndex][horizontalIndex] = fillerSymbol;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Error. Cell is out of range. Try again.");
            Scanner in = new Scanner(System.in);
            System.out.print(fillerSymbol + ", enter vertical index: ");
            int verticalIndex_new = in.nextInt();
            System.out.print(fillerSymbol + ", enter horizontal index: ");
            int horizontalIndex_new = in.nextInt();
            fillCell(movesCounter, field, verticalIndex_new, horizontalIndex_new);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Scanner in = new Scanner(System.in);
            System.out.print("Enter vertical index: ");
            int verticalIndex_new = in.nextInt();
            System.out.print("Enter horizontal index: ");
            int horizontalIndex_new = in.nextInt();
            fillCell(movesCounter, field, verticalIndex_new, horizontalIndex_new);
        }
    }
}
